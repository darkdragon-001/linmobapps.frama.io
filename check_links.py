#!/usr/bin/python3

import datetime
import sys

import pandas
import requests


def is_link(url):
    return url.strip().startswith("http")


def check_link_reachable(url):
    try:
        r = requests.get(url, allow_redirects=False)
        code = r.status_code
        if code == 200:
            return None
        elif code == 301:
            return (r.headers["Location"], f"Permanent redirect to {r.headers['Location']}")
        elif code == 302:
            return (None, f"Temporary redirect to {r.headers['Location']}")
        return (None, code)
    except Exception as e:
        return (None, e)


def check_link_https(url):
    if url.startswith("https"):
        return None

    suggestion = url.replace("http", "https")
    if check_link_reachable(suggestion) is not None:  # not reachable
        suggestion = None

    return (suggestion, "Not HTTPS!")


# check links for error
def check(row, update=False):
    checkers = [check_link_reachable, check_link_https]

    found = False
    for col in row.index:
        if is_link(row[col].strip()):  # column with links format
            links = [link.strip() for link in row[col].strip().split(",") if is_link(link)]
            found_in_links = False
            for i in range(len(links)):
                for checker in checkers:
                    result = checker(links[i])
                    if not result:  # reachable
                        continue
                    found_in_links = True
                    found_url, result_message = result
                    print(f"{links[i]}: {result_message}", file=sys.stderr)
                    if update and found_url:
                        links[i] = found_url
            if update and found_in_links:
                found = True
                row[col] = ",".join(links)
                row["updated"] = str(datetime.date.today())
                row["extra.updated_by"] = "script"
    return found


def run(filename, update=False):
    df = pandas.read_csv(filename, keep_default_na=False)
    found = False
    for idx, row in df.iterrows():
        found |= check(row, update)
    if found and update:
        print(f"Writing changes to {filename}")
        df.to_csv(filename, index=False)
    return found


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FILENAME")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_file = sys.argv[2]
    found = run(apps_file, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_file}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)
