#!/usr/bin/python3

import pathlib
import re
import sys
import tempfile

import appstream_python
import git
import pandas


def is_sourcehut(url):
    return re.match(r"^https?://(?:[^.]+\.)?sr.ht/", url) is not None


def is_qt(url):
    return re.match(r"^https?://code.qt.io/", url) is not None


def get_repository_url(repo):
    if is_sourcehut(repo):
        return re.sub(r"^https?://(?:[^.]+\.)?sr\.ht/(.*)$", r"https://git.sr.ht/\g<1>", repo).lower(), None

    if is_qt(repo):
        return re.sub(r"^https?://code\.qt\.io/(?:cgit/)?(.*\.git).*$", r"https://code.qt.io/\g<1>", repo), None

    branch = None
    if (m := re.search(r"(?:/-)?/tree/(?P<branch>.*)$", repo)) is not None:  # Github, Gitlab
        repo = repo[: m.start()]
        branch = m.groupdict().get("branch")

    if repo[-1] == "/":
        repo = repo[:-1]

    return re.match(r"^(?:[^.]*(?:\.(?!git))?)*", repo)[0] + ".git", branch


def find_appstream_xml(repo_url, branch=None, repo_path="repo"):
    try:
        kwargs = {}
        if branch is not None:
            kwargs["branch"] = branch
        repo = git.Repo.clone_from(repo_url, repo_path, multi_options=["--depth 1"], **kwargs)

        def pred(x, y):
            if x.type != "blob":
                return False

            name = pathlib.Path(x.path).name
            if "metainfo.xml" not in name and "appdata.xml" not in name:
                return False

            return True

        return [x.path for x in repo.head.commit.tree.traverse(pred)]
    except Exception as e:
        print(f"Error checking git repository {repo_url} for AppStream file: {e}", file=sys.stderr)
        return None


def load_appstream(file):
    if not file:
        return None
    app = appstream_python.AppstreamComponent()
    try:
        with open(file, "r") as f:
            app.load_file(f)
    except Exception as e:
        print(f"Error loading {file}: {e}", file=sys.stderr)
        return None
    return app


def check(row, update=False):
    item_name = row.get("extra.app_id") or row.get("name", "")
    if row.get("extra.appstream_xml_url", "").strip():
        return False

    repo, branch = get_repository_url(row["extra.repository"])
    print(f"No AppStream url found for {item_name}, checking {repo}, {branch=}", file=sys.stderr)

    with tempfile.TemporaryDirectory() as tmpdir:
        repo_path = pathlib.Path(tmpdir) / "repo"
        appstream_xml_files = find_appstream_xml(repo, branch, repo_path)
        appstream_xml_files = [appstream_xml_file for appstream_xml_file in (appstream_xml_files or []) if load_appstream(repo_path / appstream_xml_file)]
        if appstream_xml_files:
            print(f"Found AppStream metainfo files for {item_name} with {repo} at paths {', '.join(appstream_xml_files)}")

    return True


def run(filename, update=False):
    df = pandas.read_csv(filename, keep_default_na=False)
    found = False
    for idx, row in df.iterrows():
        found |= check(row, update)
    if found and update:
        print(f"Writing changes to {filename}")
        df.to_csv(filename, index=False)
    return found


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FILENAME")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_file = sys.argv[2]
    found = run(apps_file, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_file}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)
