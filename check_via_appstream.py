#!/usr/bin/python3

import datetime
import sys
import traceback

import appstream_python
import markdownify
import pandas
import requests


def load_appstream(url):
    if not url:
        return None
    app = appstream_python.AppstreamComponent()
    try:
        response = requests.get(url)
        if not response.ok:
            print(f"Error loading {url}", file=sys.stderr)
            return None
        app.load_bytes(response.content, encoding=response.encoding)
    except Exception as e:
        print(f"Error loading {url}: {e}", file=sys.stderr)
        return None
    return app


def get_appstream_app_id(app):
    return app.id


def get_appstream_name(app):
    return app.name.get_default_text()


def get_appstream_categories(app):
    return ",".join(app.categories)


def get_appstream_app_author(app):
    return app.developer_name.get_default_text()


def get_appstream_metadata_licenses(app):
    return ",".join(app.metadata_license.split("AND"))


def get_appstream_project_licenses(app):
    return ",".join(app.project_license.split("AND"))


def get_appstream_summary(app):
    return app.summary.get_default_text()


def get_appstream_description(app):
    return markdownify.markdownify(app.description.to_html(lang=None), heading_style="ATX")


def get_appstream_screenshots(app):
    return ",".join([screenshot.get_source_image().url.strip() for screenshot in app.screenshots if screenshot.get_source_image() is not None])


# possible URL types: https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-url
def get_appstream_url(app, url_type):
    return app.urls.get(url_type, "") or ""  # filter out empty <url type="TYPE"></url>


def check(row, update=False):
    item_name = row.get("extra.app_id") or row.get("name", "")
    app = load_appstream(row["extra.appstream_xml_url"])
    if not app:
        return False

    properties = [
        {"apps_csv_column": "extra.homepage", "handler": lambda app: get_appstream_url(app, "homepage")},
        {"apps_csv_column": "extra.bugtracker", "handler": lambda app: get_appstream_url(app, "bugtracker")},
        {"apps_csv_column": "extra.donations", "handler": lambda app: get_appstream_url(app, "donation")},
        {"apps_csv_column": "extra.translations", "handler": lambda app: get_appstream_url(app, "translate")},
        {"apps_csv_column": "extra.repository", "handler": lambda app: get_appstream_url(app, "vcs-browser")},
        {"apps_csv_column": "extra.app_id", "handler": get_appstream_app_id},
        {"apps_csv_column": "name", "handler": get_appstream_name},
        {"apps_csv_column": "[taxonomies.category]", "handler": get_appstream_categories},
        {"apps_csv_column": "[taxonomies.app_author]", "handler": get_appstream_app_author},
        {"apps_csv_column": "[taxonomies.metadata_licenses]", "handler": get_appstream_metadata_licenses},
        {"apps_csv_column": "[taxonomies.project_licenses]", "handler": get_appstream_project_licenses},
        {"apps_csv_column": "summary", "handler": get_appstream_summary},
        {"apps_csv_column": "description", "handler": get_appstream_description},
        {"apps_csv_column": "[extra.screenshots]", "handler": get_appstream_screenshots},
    ]
    found = False
    for property in properties:
        try:
            found_entry = property["handler"](app).strip()
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_csv_column"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

        if row.get(property["apps_csv_column"]) and not found_entry:
            print(f'{item_name}: {property["apps_csv_column"]} missing in upstream AppStream file. Consider contributing it upstream: {row[property["apps_csv_column"]]}', file=sys.stderr)
        if not found_entry or found_entry == row.get(property["apps_csv_column"]):
            continue  # already up to date

        message = f'{item_name}: {property["apps_csv_column"]} '
        if not row.get(property["apps_csv_column"]):
            message += "new: "
        else:
            message += f'outdated {row[property["apps_csv_column"]]} -> '
        message += found_entry
        print(message, file=sys.stderr)

        found = True
        if update:
            row[property["apps_csv_column"]] = found_entry
            if row.get(source_column := (property["apps_csv_column"] + "_source")):
                if row[source_column] != row["appstream_xml_url"]:
                    print(f'{item_name}: {source_column} {row[source_column]} -> {row["appstream_xml_url"]}', file=sys.stderr)
                row[source_column] = row["appstream_xml_url"]
            row["updated"] = str(datetime.date.today())
            row["extra.updated_by"] = "script"

    return found


def run(filename, update=False):
    df = pandas.read_csv(filename, keep_default_na=False)
    found = False
    for idx, row in df.iterrows():
        found |= check(row, update)
    if found and update:
        print(f"Writing changes to {filename}")
        df.to_csv(filename, index=False)
    return found


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FILENAME")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_file = sys.argv[2]
    found = run(apps_file, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_file}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)
